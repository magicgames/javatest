package com.avoris.demo.service;

import com.avoris.demo.model.Todo;
import java.util.List;

public interface TodoService {

	Todo findById(long id);
	void saveTodo(Todo todo);
	void updateTodo(Todo todo);
	void deleteTodoById(long id);
	List<Todo> findAllTodos();

}
