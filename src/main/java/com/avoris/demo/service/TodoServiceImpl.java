package com.avoris.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.avoris.demo.model.Todo;


@Service("todoService")
public class TodoServiceImpl implements TodoService {
    public static final Logger logger = LoggerFactory.getLogger(TodoServiceImpl.class);
    private static final AtomicLong counter = new AtomicLong();
    private static List<Todo> todos = new ArrayList<Todo>();

    public Todo findById(long id) {
        logger.info("Fetching User with id {}", id);
        for(Todo todo : todos){
            if(todo.getId() == id){
                return todo;
            }
        }
        return null;
    }




    public void saveTodo(Todo todo) {
        todo.setId(counter.incrementAndGet());
        todos.add(todo);
    }

    public void updateTodo(Todo todo) {
        int index = todos.indexOf(todo);
        todos.set(index, todo);
    }


    public void deleteTodoById(long id) {
        todos.removeIf(todo -> todo.getId() == id);
    }


    public List<Todo> findAllTodos() {

        return todos;
    }

}



