package com.avoris.demo.model;

public class Todo {

	private long id;

	private String title;

	private String description;

	private Boolean isCompleted;

	public Todo() {
		id = 0;
	}

	public Todo(long id, String title, String description, Boolean isCompleted) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.isCompleted = isCompleted;
	}

	public Todo(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Todo(long id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsCompleted() {
		return isCompleted;
	}

	public void setIsCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Todo other = (Todo) obj;
		return (id == other.id);

	}

	@Override
	public String toString() {
		return "Todo [id=" + id + ", title=" + title + " , description = "+ description +"]";

	}


}
