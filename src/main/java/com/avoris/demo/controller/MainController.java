package com.avoris.demo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.avoris.demo.model.Todo;
import com.avoris.demo.service.TodoService;
import com.avoris.demo.util.CustomError;

@Controller    // This means that this class is a Controller
@RequestMapping(path = "/api")


public class MainController {
    @Autowired
    TodoService todoService;

    public static final Logger logger = LoggerFactory.getLogger(MainController.class);


    @RequestMapping(value = "/todo/", method = RequestMethod.GET)
    public ResponseEntity<List<Todo>> listAllTodos() {
        List<Todo> todos = todoService.findAllTodos();
        if (todos.isEmpty()) {
            logger.info("No TODOS");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            // You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<>(todos, HttpStatus.OK);
    }

    @RequestMapping(value = "/todo/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getTodo(@PathVariable("id") long id) {
        logger.info("Fetching Todo with id {}", id);
        Todo todo = todoService.findById(id);
        if (todo == null) {
            logger.error("User with id {} not found.", id);
            return new ResponseEntity<>(new CustomError("Todo with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(todo, HttpStatus.OK);
    }

    @RequestMapping(value = "/todo/", method = RequestMethod.POST)
    public ResponseEntity<?> createTodo(@RequestBody Todo todo, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Todo : {}", todo);
        todoService.saveTodo(todo);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/todo/{id}").buildAndExpand(todo.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/todo/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> patchTodo(@PathVariable("id") long id, @RequestBody Todo todo) {
        logger.info("Patch Todo with id {}", id);

        Todo currentTodo = todoService.findById(id);

        if (currentTodo == null) {
            logger.error("Unable to update. Todo with id {} not found.", id);
            return new ResponseEntity<>(new CustomError("Unable to patch. User with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        if (todo.getTitle() != null) {
            currentTodo.setTitle(todo.getTitle());
        }

        if (todo.getDescription() != null) {
            currentTodo.setDescription(todo.getDescription());
        }
        if (todo.getIsCompleted() != null) {
            currentTodo.setIsCompleted(todo.getIsCompleted());
        }


        if (todo.getTitle()  == null || todo.getDescription() == null || todo.getIsCompleted() == null) {
            currentTodo.setTitle(todo.getTitle());
        }


        todoService.updateTodo(currentTodo);
        return new ResponseEntity<Todo>(currentTodo, HttpStatus.OK);
    }


    @RequestMapping(value = "/todo/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateTodo(@PathVariable("id") long id, @RequestBody Todo todo) {
        logger.info("Updating Todo with id {}", id);

        Todo currentTodo = todoService.findById(id);

        if (currentTodo == null) {
            logger.error("Unable to update. Todo with id {} not found.", id);
            return new ResponseEntity<>(new CustomError("Unable to upate. User with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        if (todo.getTitle()  == null || todo.getDescription() == null || todo.getIsCompleted() == null) {
            return new ResponseEntity<>(new CustomError("Faltan parámetros"),HttpStatus.BAD_REQUEST);
        }
        currentTodo.setTitle(todo.getTitle());
        currentTodo.setDescription(todo.getDescription());
        currentTodo.setIsCompleted(todo.getIsCompleted());
        todoService.updateTodo(currentTodo);
        return new ResponseEntity<Todo>(currentTodo, HttpStatus.OK);
    }



    @RequestMapping(value = "/todo/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTodo(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting User with id {}", id);

        Todo currenTodo = todoService.findById(id);
        if (currenTodo == null) {
            logger.error("Unable to delete. Todo with id {} not found.", id);
            return new ResponseEntity<>(new CustomError("Unable to delete. Todo with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        todoService.deleteTodoById(currenTodo.getId());
        return new ResponseEntity<Todo>(HttpStatus.NO_CONTENT);
    }



}
