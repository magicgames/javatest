package com.avoris.demo;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;

import com.avoris.demo.model.Todo;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;


public class DemoApplicationTests {

    public static final String REST_SERVICE_URI = "http://localhost:8080/api";

    /* GET */
    @SuppressWarnings("unchecked")
    private static void listAllTodos() {
        System.out.println("Test list TODOs -----------");

        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> todosMap = restTemplate.getForObject(REST_SERVICE_URI + "/todo/", List.class);

        if (todosMap != null) {
            for (LinkedHashMap<String, Object> map : todosMap) {
                System.out.println("Todo : id=" + map.get("id") + ", Title=" + map.get("title") +
                        ", Description=" + map.get("description") + ", isCompleted=" + map.get("isCompleted"));

            }
        } else {
            System.out.println("No user exist----------");
        }
    }

    /* GET */
    private static void getTodo() {
        System.out.println("Testing getTodo API----------");
        RestTemplate restTemplate = new RestTemplate();
        Todo todo = restTemplate.getForObject(REST_SERVICE_URI + "/todo/1", Todo.class);
        System.out.println(todo);
    }

    /* POST */
    private static void createTodo() {
        System.out.println("Testing create Todo API----------");
        RestTemplate restTemplate = new RestTemplate();
        Todo todo  = new Todo(0, "titulo", "Mola este test", false);
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI + "/todo/", todo, Todo.class);
        System.out.println("Location : " + uri.toASCIIString());
    }



    private static void updateTodo() {
        System.out.println("Testing update Todo API----------");
        RestTemplate restTemplate = new RestTemplate();
        Todo todo = new Todo(1, "tituloUpdate", "desc2", true);
        restTemplate.put(REST_SERVICE_URI + "/todo/1/",todo);
        System.out.println(todo);
    }

    private static void patchTodo1() {
        System.out.println("Testing update Todo API----------");
        Todo todo = new Todo(1, "tituloPatch1");
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "merge-patch+json");
        headers.setContentType(mediaType);
        HttpEntity<Todo> requestPatch1 = new HttpEntity<>(todo,headers);
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        restTemplate.exchange(REST_SERVICE_URI + "/todo/1", HttpMethod.PATCH, requestPatch1, Void.class);

    }


    private static void patchTodo2() {
        System.out.println("Testing update Todo API----------");

        Todo todo = new Todo(1, "tituloPatch2","dectPatch2");
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "merge-patch+json");
        headers.setContentType(mediaType);
        HttpEntity<Todo> requestPatch1 = new HttpEntity<>(todo,headers);
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        restTemplate.exchange(REST_SERVICE_URI + "/todo/2", HttpMethod.PATCH, requestPatch1, Void.class);

    }
    private static void deleteTodo() {
        System.out.println("Testing delete User API----------");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI + "/todo/1");
    }

    public static void main(String args[]) {
        createTodo();
        createTodo();
        listAllTodos();
        getTodo();
        updateTodo();
        listAllTodos();
        patchTodo1();
        listAllTodos();
        patchTodo2();
        listAllTodos();
        deleteTodo();
        listAllTodos();
    }
}
