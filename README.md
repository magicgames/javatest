## Ejemplo básico de API

Se han implementado los métodos ( GET , POST , PUT , PATCH)

* Listado de todos los todos **/api/todos** (GET)
* Obtención de 1 todo  **/api/todos/{id}**  (GET)
* Creación de nuevo todo **/api/todos/** (POST)
* Actualización completa de un todo **/api/todos/{id}**  (PUT)
* Actualización parcial de un todo **/api/todos/{id}**  (PATCH)


## Ramas del proyecto
* La rama **master** del proyecto tiene todos los TODO en un array_list
* la rama **tree_map** tiene una modificacion del repositorio para implementar treeMap
* La rama **tree_map** tiene un sistema de test con 100% coverage
* La rama **tree_map** corrige ciertos bugs en el controlador 